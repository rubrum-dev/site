<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PageController extends Controller
{
    public function index()
    {
        return view('pages.index');
    }

    public function pages($page)
    {
        return view('pages.' . $page);
    }
}
