<!-- Features Section -->
<section class="page-section">
    <div class="container relative">
        <h2 class="section-title font-alt mb-70 mb-sm-40">NOSSOS PRINCÍPIOS</h2>
        <!-- Features Grid -->
        <div class="row multi-columns-row alt-features-grid">
            <!-- Features Item -->
            <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="alt-features-item align-center">
                    <div class="alt-features-icon">
                        <span class="icon-flag"></span>
                    </div>
                    <h3 class="alt-features-title font-alt">Integridade Legal</h3>
                    <div class="alt-features-descr align-left">
                        Trabalhamos com coerência, transparência, ética e respeito máximo às leis e convenções nacionais e internacionais.
                    </div>
                </div>
            </div>
            <!-- End Features Item -->

            <!-- Features Item -->
            <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="alt-features-item align-center">
                    <div class="alt-features-icon">
                        <span class="icon-chat"></span>
                    </div>
                    <h3 class="alt-features-title font-alt">Busca da convergência</h3>
                    <div class="alt-features-descr align-left">
                        Acreditamos que escutar opiniões diferentes são sempre enriquecedoras e quando somadas à nossa experiência, atingimos
                        o melhor resultado.
                    </div>
                </div>
            </div>
            <!-- End Features Item -->

            <!-- Features Item -->
            <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="alt-features-item align-center">
                    <div class="alt-features-icon">
                        <span data-icon="&#xe04e"></span>
                    </div>
                    <h3 class="alt-features-title font-alt">Paixão pela Excelência</h3>
                    <div class="alt-features-descr align-left">
                        Somos obcecados por pela excelência, nos entregamos de corpo e alma aos trabalhos para vê-los perfeitos nas mãos dos clientes.
                    </div>
                </div>
            </div>
            <!-- End Features Item -->

            <!-- Features Item -->
            <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="alt-features-item align-center">
                    <div class="alt-features-icon">
                        <span class="icon-strategy"></span>
                    </div>
                    <h3 class="alt-features-title font-alt">Rapidez e Determinação</h3>
                    <div class="alt-features-descr align-left">
                        Buscamos a assertividade e agilidade nas tomadas de decisão, visando sempre o melhor para nossos projetos de maneiras prática
                        e objetiva.
                    </div>
                </div>
            </div>
            <!-- End Features Item -->

            <!-- Features Item -->
            <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="alt-features-item align-center">
                    <div class="alt-features-icon">
                        <span class="icon-lightbulb"></span>
                    </div>
                    <h3 class="alt-features-title font-alt">Propositivos e Proativos</h3>
                    <div class="alt-features-descr align-left">
                        Vamos além do exigido, com iniciativa, autonomia e criatividade, nossa meta é entregar um projeto melhor do que aquele
                        que recebemos.
                    </div>
                </div>
            </div>
            <!-- End Features Item -->

            <!-- Features Item -->
            <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="alt-features-item align-center">
                    <div class="alt-features-icon">
                        <span class="icon-heart"></span>
                    </div>
                    <h3 class="alt-features-title font-alt">De Pessoas para Pessoas</h3>
                    <div class="alt-features-descr align-left">
                        Utilizamos maquinário de ponta, mas nosso trabalho é feito para pessoas, e por isso estimamos laços pessoais entre todas
                        os participantes de cada projeto.
                    </div>
                </div>
            </div>
            <!-- End Features Item -->
        </div>
        <!-- End Features Grid -->

    </div>
</section>
<!-- End Features Section -->
