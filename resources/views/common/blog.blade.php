<!-- Blog Section -->
<section class="page-section" id="red">
    <div class="container relative">

        <h2 class="section-title font-alt align-left mb-70 mb-sm-40">
            Latest News

            <a href="blog-classic-sidebar-right.html" class="section-more right">All News in our blog <i class="fa fa-angle-right"></i></a>

        </h2>

        <div class="row multi-columns-row">

            <!-- Post Item -->
            <div class="col-sm-6 col-md-4 col-lg-4 mb-md-50 wow fadeIn" data-wow-delay="0.1s" data-wow-duration="2s">

                <div class="post-prev-img">
                    <a href="blog-single-sidebar-right.html"><img src="images/blog/post-prev-1.jpg" alt="" /></a>
                </div>

                <div class="post-prev-title font-alt">
                    <a href="">New Web Design Trends</a>
                </div>

                <div class="post-prev-info font-alt">
                    <a href="">John Doe</a> | 10 December
                </div>

                <div class="post-prev-text">
                    Maecenas  volutpat, diam enim sagittis quam, id porta quam. Sed id dolor
                    consectetur fermentum nibh volutpat, accumsan purus.
                </div>

                <div class="post-prev-more">
                    <a href="" class="btn btn-mod btn-gray btn-round">Read More <i class="fa fa-angle-right"></i></a>
                </div>

            </div>
            <!-- End Post Item -->

            <!-- Post Item -->
            <div class="col-sm-6 col-md-4 col-lg-4 mb-md-50 wow fadeIn" data-wow-delay="0.2s" data-wow-duration="2s">

                <div class="post-prev-img">
                    <a href="blog-single-sidebar-right.html"><img src="images/blog/post-prev-2.jpg" alt="" /></a>
                </div>

                <div class="post-prev-title font-alt">
                    <a href="">Minimalistic Design Forever</a>
                </div>

                <div class="post-prev-info font-alt">
                    <a href="">John Doe</a> | 9 December
                </div>

                <div class="post-prev-text">
                    Maecenas  volutpat, diam enim sagittis quam, id porta quam. Sed id dolor
                    consectetur fermentum nibh volutpat, accumsan purus.
                </div>

                <div class="post-prev-more">
                    <a href="" class="btn btn-mod btn-gray btn-round">Read More <i class="fa fa-angle-right"></i></a>
                </div>

            </div>
            <!-- End Post Item -->

            <!-- Post Item -->
            <div class="col-sm-6 col-md-4 col-lg-4 mb-md-50 wow fadeIn" data-wow-delay="0.3s" data-wow-duration="2s">

                <div class="post-prev-img">
                    <a href="blog-single-sidebar-right.html"><img src="images/blog/post-prev-3.jpg" alt="" /></a>
                </div>

                <div class="post-prev-title font-alt">
                    <a href="">Hipster&rsquo;s Style in&nbsp;Web</a>
                </div>

                <div class="post-prev-info font-alt">
                    <a href="">John Doe</a> | 7 December
                </div>

                <div class="post-prev-text">
                    Maecenas  volutpat, diam enim sagittis quam, id porta quam. Sed id dolor
                    consectetur fermentum nibh volutpat, accumsan purus.
                </div>

                <div class="post-prev-more">
                    <a href="" class="btn btn-mod btn-gray btn-round">Read More <i class="fa fa-angle-right"></i></a>
                </div>

            </div>
            <!-- End Post Item -->

        </div>

    </div>
</section>
<!-- End Blog Section -->


<!-- Newsletter Section -->
<section class="small-section bg-gray-lighter">
    <div class="container relative">

        <form class="form align-center" id="mailchimp">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <div class="newsletter-label font-alt">
                        Stay informed with our newsletter
                    </div>

                    <div class="mb-20">
                        <input placeholder="Enter Your Email" class="newsletter-field form-control input-md round mb-xs-10" type="email" pattern=".{5,100}" required/>

                        <button type="submit" class="btn btn-mod btn-medium btn-round mb-xs-10">
                            Subscribe
                        </button>
                    </div>

                    <div class="form-tip">
                        <i class="fa fa-info-circle"></i> Please trust us, we will never send you spam
                    </div>

                    <div id="subscribe-result"></div>

                </div>
            </div>
        </form>

    </div>
</section>
<!-- End Newsletter Section -->
