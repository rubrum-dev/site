<!-- Testimonials Section -->
<section class="page-section bg-dark bg-dark-alfa-90 fullwidth-slider" data-background="#000000">
    <!-- Slide Item -->
    <div>
        <div class="container relative">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 align-center">
                    <!-- Section Icon -->
                    <div class="section-icon">
                        <span class="icon-quote"></span>
                    </div>
                    <!-- Section Title --><h3 class="small-title font-alt">O QUE NOSSOS PARCEIROS DIZEM?</h3>
                    <blockquote class="testimonial white">
                        <p>
                            A PaintPack é sempre minha primeira opção quando tenho uma inovação de produto, eles me ajudam
                            a tangibilizar a ideia com material eficaz, altissima qualidade e excelente atendimento.
                        </p>
                        <footer class="testimonial-author">
                            TACIANA AVILA, GERENTE SKOL - AMBEV
                        </footer>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>
    <!-- End Slide Item -->

    <!-- Slide Item -->
    <div>
        <div class="container relative">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 align-center">
                    <!-- Section Icon -->
                    <div class="section-icon">
                        <span class="icon-quote"></span>
                    </div>
                    <!-- Section Title --><h3 class="small-title font-alt">O QUE NOSSOS PARCEIROS DIZEM?</h3>
                    <blockquote class="testimonial white">
                        <p>
                            Com grande agilidade na execução, a Paint Pack entrega mock-ups fiéis aos produtos finais.
                            A diversidade de formatos oferecidos pela empresa permite reproduzir em detalhes
                            embalagens complexas como latas, potes plásticos e pouchs.
                        </p>
                        <footer class="testimonial-author">
                            Thiago Francisco, GERRENTE DE PRODUTO - CIA CACIQUE DE CAFÉ SOLÚVEL
                        </footer>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>
    <!-- End Slide Item -->

    <!-- Slide Item -->
    <div>
        <div class="container relative">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 align-center">
                    <!-- Section Icon -->
                    <div class="section-icon">
                        <span class="icon-quote"></span>
                    </div>
                    <!-- Section Title --><h3 class="small-title font-alt">O QUE NOSSOS PARCEIROS DIZEM?</h3>
                    <blockquote class="testimonial white">
                        <p>
                            Trabalhei com a PaintPack em diferentes momentos e, em todos eles, fomos atendidos com agilidade e qualidade.
                            É uma empresa super parceira para qualquer projeto, sempre disposta a ajudar e encarar missões impossíveis.
                            Qualidade indispensável para quem quer inovar e fazer melhor.
                        </p>
                        <footer class="testimonial-author">
                            Alessandra Volkweis, atendimento - Agência DM9DDB
                        </footer>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>
    <!-- End Slide Item -->
</section>
<!-- End Testimonials Section -->
