@extends('common.template')

@section('title')
	Início
@stop

@section('content')

        <!-- Fullwidth Slider -->
        <div class="home-section fullwidth-slider-fade bg-dark" id="home">

            <!-- Slide Item -->
            <section class="home-section bg-scroll bg-dark-alfa-50" data-background="images/full-width-images/destaque-home.jpg">
                <div class="js-height-full container">

                    <!-- Hero Content -->
                    <div class="home-content">
                        <div class="home-text">

                            <h1 class="hs-line-8 no-transp font-alt mb-50 mb-xs-30">
                                Branding / Design / Development / Photo
                            </h1>

                            <h2 class="hs-line-14 font-alt mb-50 mb-xs-30">
                                Design Studio
                            </h2>

                            <div class="local-scroll">
                                <a href="#about" class="btn btn-mod btn-border-w btn-medium btn-round hidden-xs">See More</a>
                                <span class="hidden-xs">&nbsp;</span>
                                <a href="http://vimeo.com/50201327" class="btn btn-mod btn-border-w btn-medium btn-round lightbox mfp-iframe">Play Reel</a>
                            </div>

                        </div>
                    </div>
                    <!-- End Hero Content -->

                </div>
            </section>
            <!-- End Slide Item -->

            <!-- Slide Item -->
            <section class="home-section bg-scroll bg-dark-alfa-30" data-background="images/full-width-images/destaque-home.jpg">
                <div class="js-height-full container">

                    <!-- Hero Content -->
                    <div class="home-content">
                        <div class="home-text">

                            <h1 class="hs-line-8 no-transp font-alt mb-50 mb-xs-30">
                                We Are just crative people
                            </h1>

                            <h2 class="hs-line-14 font-alt mb-50 mb-xs-30">
                                Rhythm Creative Studio
                            </h2>

                            <div class="local-scroll">
                                <a href="#about" class="btn btn-mod btn-border-w btn-medium btn-round hidden-xs">See More</a>
                                <span class="hidden-xs">&nbsp;</span>
                                <a href="http://vimeo.com/50201327" class="btn btn-mod btn-border-w btn-medium btn-round lightbox mfp-iframe">Play Reel</a>
                            </div>

                        </div>
                    </div>
                    <!-- End Hero Content -->

                </div>
            </section>
            <!-- End Slide Item -->

            <!-- Slide Item -->
            <section class="home-section bg-scroll bg-dark-alfa-30" data-background="images/full-width-images/destaque-home.jpg">
                <div class="js-height-full container">

                    <!-- Hero Content -->
                    <div class="home-content">
                        <div class="home-text">

                            <h2 class="hs-line-8 no-transp font-alt mb-50 mb-xs-30">
                                Create your dream with
                            </h2>

                            <h1 class="hs-line-14 font-alt mb-50 mb-xs-30">
                                Amazing Design
                            </h1>

                            <div>
                                <a href="pages-pricing-1.html" class="btn btn-mod btn-border-w btn-medium btn-round">Get Pricing</a>
                            </div>

                        </div>
                    </div>
                    <!-- End Hero Content -->

                </div>
            </section>
            <!-- End Slide Item -->

        </div>
        <!-- End Fullwidth Slider -->

        <!-- Home Section -->
        {{-- <section class="home-section bg-dark-alfa-30 parallax-2" data-background="images/full-width-images/destaque-home.jpg" id="home">
            <div class="js-height-full">

                <!-- Hero Content -->
                <div class="home-content">
                    <div class="home-text">

                        <h1 class="hs-line-1 font-alt mb-80 mb-xs-30 mt-70 mt-sm-0 destaque-tex">
                            BEM-VINDO AO NOVO SITE
                        </h1>

                        <div class="hs-line-6">
                            NOVAS CORES<br />
                            NOVOS ARES
                        </div>

                    </div>
                </div>
                <!-- End Hero Content -->

                <!-- Scroll Down -->
                <div class="local-scroll">
                    <a href="#dry-offset" class="scroll-down"><i class="fa fa-angle-down scroll-down-icon"></i></a>
                </div>
                <!-- End Scroll Down -->

            </div>
        </section> --}}
        <!-- End Home Section -->

        <!-- About Section -->
        <section class="page-section" id="dry-offset">
            <div class="container relative">

                <h2 class="section-title font-alt align-left mb-70 mb-sm-40">
                    NOSSOS PILARES
                </h2>

                <div class="section-text mb-sm-20">
                    <div class="row">

                        <div class="col-md-4">
                            <blockquote>
                                <p>
                                “Viabilizar a produção industrial de embalagens com perfeição e gerar economia para nossos clientes, isso que nos move”
                                </p>
                                <footer>
                                    Rodrigo Korovichenco, ceo paintpack
                                </footer>
                            </blockquote>
                        </div>

                        <div class="col-md-4 col-sm-6 mb-sm-50 mb-xs-30">
                            Há mais de uma década no mercado de embalagens, a PaintPack oferece soluções customizadas, visando garantir que o design da embalagem seja feito com máxima qualidade na produção industrial.
                            Com empresas locais e multinacionais entre nossos parceiros, hoje atendemos clientes em toda América Latina com escritórios em São Paulo e Santiago.
                        </div>

                        <div class="col-md-4 col-sm-6 mb-sm-50 mb-xs-30">
                            Contamos com uma equipe altemente especializada e em constanete capacitação para garantir sempre o melhor resultado. Com maquinário de ponta em nosso QG,
                            troxemos a primeira e mais avançada impressora de latas de amostra do Brasil, com capacidade de imprimir até 8 cores e em qualquer formato de lata de alumínio.
                        </div>

                    </div>
                </div>

                <div class="row">

                    <!-- Team item -->
                    <div class="col-sm-4 mb-xs-30 wow fadeInUp">
                        <div class="team-item">

                            <div class="team-item-descr font-alt">

                                <div class="team-item-name">
                                    DRY OFFSET
                                </div>

                            </div>
							<a href="/dryoffset">
	                            <div class="team-item-image">

										<img src="images/team/pilar-1.jpg" alt="" />

		                                <div class="team-item-detail">
		                                    <p>
		                                        LATAS DE AMOSTRA<br />
		                                        FILMES NEGATIVOS<br />
		                                        PLACAS<br />
		                                        CARTELA DE CORES
		                                    </p>
		                                </div>
	                            </div>
							</a>

                        </div>
                    </div>
                    <!-- End Team item -->

                    <!-- Team item -->
                    <div class="col-sm-4 mb-xs-30 wow fadeInUp">
                        <div class="team-item">

                            <div class="team-item-descr font-alt">

                                <div class="team-item-name">
                                    MOCK-UP
                                </div>

                            </div>
							<a href="/mockup">
	                            <div class="team-item-image">

	                                <img src="images/team/pilar-2.jpg" alt="" />

	                                <div class="team-item-detail">
	                                    <p>
	                                        PESQUISA DE MERCADO<br />
	                                        FOTO PUBLICITÁRIA<br />
	                                        FILMES COMERCIAIS<br />
	                                        AVALIAÇÃO DE LAYOUT
	                                    </p>
	                                </div>
	                            </div>
							</a>

                        </div>
                    </div>
                    <!-- End Team item -->

                    <!-- Team item -->
                    <div class="col-sm-4 mb-xs-30 wow fadeInUp">
                        <div class="team-item">

                            <div class="team-item-descr font-alt">

                                <div class="team-item-name">
                                    PRE-MEDIA
                                </div>

                            </div>
							<a href="/premedia">
	                            <div class="team-item-image">

	                                <img src="images/team/pilar-3.jpg" alt="" />

	                                <div class="team-item-detail">
	                                    <p>
	                                        ORGANIZAÇÃO<br />
	                                        DOS FLUXOS<br />
	                                        BRAND-TO-PACKAGE
	                                    </p>
	                                </div>
	                            </div>
							</a>

                        </div>
                    </div>
                    <!-- End Team item -->

                </div>


            </div>
        </section>
        <!-- End About Section -->

        <!-- Divider -->
        <hr class="mt-0 mb-0 "/>
        <!-- End Divider -->

        <!-- Services Section -->
        <section class="page-section" id="mock-up">
            <div class="container relative">

                <h2 class="section-title font-alt mb-70 mb-sm-40">
                    Serviços
                </h2>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs tpl-alt-tabs font-alt pt-30 pt-sm-0 pb-30 pb-sm-0">
                    <li class="active">
                        <a href="#service-mock-up" data-toggle="tab">

                            <div class="alt-tabs-icon">
                                <span class="icon-camera"></span>
                            </div>

                            Mock-ups
                        </a>
                    </li>
                    <li>
                        <a href="#service-lata-amostra" data-toggle="tab">

                            <div class="alt-tabs-icon">
                                <span class="icon-trophy"></span>
                            </div>

                            Latas de Amostra
                        </a>
                    </li>
                    <li>
                        <a href="#service-fluxos-aprovacao" data-toggle="tab">

                            <div class="alt-tabs-icon">
                                <span class="icon-linegraph"></span>
                            </div>

                            Fluxos de Aprovação
                        </a>
                    </li>
                    <li>
                        <a href="#service-matrizes-impressao" data-toggle="tab">

                            <div class="alt-tabs-icon">
                                <span class="icon-scope"></span>
                            </div>

                            Matrizes de Impressão
                        </a>
                    </li>
                </ul>
                <!-- End Nav tabs -->

                <!-- Tab panes -->
                <div class="tab-content tpl-tabs-cont">

                    <!-- Service Item -->
                    <div class="tab-pane fade in active" id="service-mock-up">

                        <div class="section-text">
                            <div class="row">
                                <div class="col-md-4 mb-md-40 mb-xs-30">
                                    <blockquote class="mb-0">
                                        <p>
                                            Transformar a ideia em realidade através de um mock-up é a melhor forma de avaliar a aplicabilidade de um projeto.
                                        </p>
                                    </blockquote>
                                </div>
                                <div class="col-md-4 col-sm-6 mb-sm-50 mb-xs-30">
                                    Mock-up é solução ideal para dar vida a uma ideia, um projeto.
                                    Possui inúmeras utilidades no mercado de embalagem, seja nas mãos dos gestores de produtos, agências ou de clientes.
                                    A PaintPack produz mock-ups para qualquer tipo de embalagem física ou em 3D.
                                </div>
                                <div class="col-md-4 col-sm-6 mb-sm-50 mb-xs-30">
                                    A qualidade da impressão e acabamento dos mock-ups físicos da PaintPack nos tornou referência para o mercado.
                                    Nossos mock-ups virtuais em 3D são interativos, podem ser girados em 360° para avaliar a embalagem por completo.
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- End Service Item -->

                    <!-- Service Item -->
                    <div class="tab-pane fade" id="service-lata-amostra">

                        <div class="section-text">
                            <div class="row">
                                <div class="col-md-4 mb-md-40 mb-xs-30">
                                    <blockquote class="mb-0">
                                        <p>
                                            Antes da produção industrial, o teste de impressão dry offset mais preciso é feito através da lata de amostra.
                                        </p>
                                    </blockquote>
                                </div>
                                <div class="col-md-4 col-sm-6 mb-sm-50 mb-xs-30">
                                    Dispomos de uma máquina com capacidade de impressão de 6 cores, que entrega uma lata idêntica à que será produzida em escala industrial.
									Somos a primeira empresa a trazer essa tecnologia para o Brasil.
                                </div>
                                <div class="col-md-4 col-sm-6 mb-sm-50 mb-xs-30">
                                    A lata de amostra viabiliza ajustes antes da produção final, pois revela tanto a fidelidade das as cores escolhidas pela criação,
									quanto as melhores opções para degradês e níveis de transparência ou opacidade da tinta sobre o alumínio.
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- End Service Item -->

                    <!-- Service Item -->
                    <div class="tab-pane fade" id="service-fluxos-aprovacao">

                        <div class="section-text">
                            <div class="row">
                                <div class="col-md-4 mb-md-40 mb-xs-30">
                                    <blockquote class="mb-0">
                                        <p>
                                            A sequência de aprovação correta é essencial para o sucesso de um projeto, reduz erros e otimiza resultados.
                                        </p>
                                    </blockquote>
                                </div>
                                <div class="col-md-4 col-sm-6 mb-sm-50 mb-xs-30">
                                    Desde o recebimento do arquivo até a versão final, coordenamos um minucioso fluxo de aprovação para que nada saia
									diferente do planejado no que tange à separação de cores, aplicação de códigos de barras e ajustes de volume e corpo dos textos legais.
                                </div>
                                <div class="col-md-4 col-sm-6 mb-sm-50 mb-xs-30">
                                    Dessa forma garantimos que a arte-final seja entregue sem erros e dessa forma otimizando os resultados do projeto.
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- End Service Item -->

                    <!-- Service Item -->
                    <div class="tab-pane fade" id="service-matrizes-impressao">

                        <div class="section-text">
                            <div class="row">
                                <div class="col-md-4 mb-md-40 mb-xs-30">
                                    <blockquote class="mb-0">
                                        <p>
                                            Vamos do início ao fim do projeto, gravamos placas digitais de alta qualidade CDI Esko para impressão industrial.
                                        </p>
                                    </blockquote>
                                </div>
                                <div class="col-md-4 col-sm-6 mb-sm-50 mb-xs-30">
                                    Depois de concluir todo o fluxo de aprovação, a PaintPack fornece a gravação da arte-final em placas digitais CDI Esko
									para impressão industrial, e ainda trabalha também com filmes de qualidade para placas analógicas.
                                </div>
                                <div class="col-md-4 col-sm-6 mb-sm-50 mb-xs-30">
                                    Assim vamos do início ao final do ciclo, garantindo a máxima qualidade na entrega do projeto, nosso principal compromisso
									com nossos clientes e parceiros.
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Service Item -->
                </div>
                <!-- End Tab panes -->
            </div>
        </section>
        <!-- End Services Section -->

        <!-- Call Action Section -->
        <section class="page-section pt-0 pb-0 banner-section bg-dark" data-background="#000000">
            <div class="container relative">
                <div class="row">
                    <div class="col-sm-7">
                        <div class="mt-140 mt-lg-80 mb-140 mb-lg-80">
                            <div class="banner-content">
                                <h3 class="banner-heading font-alt">QUER TER CERTEZA? FAÇA UMA LATA DE AMOSTRA.</h3>
                                <div class="banner-decription">
                                    - Revela se as cores finais correspondem com as escolhidas pela criação<br />
                                    - Determina as melhores opções para degradês<br />
                                    - Avalia a relação da transparência ou opacidade da tinta sobre o alumínio
                                </div>
                                <div class="local-scroll">
                                    <a href="#contato" class="btn btn-mod btn-w btn-medium btn-round">Veja Mais</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 banner-image wow fadeInUp">
                        <img src="images/banner-servicos.png" alt="" />
                    </div>
                </div>
            </div>
        </section>
        <!-- End Call Action Section -->

        <!-- Clientes e Parceiros Section -->
        <section class="page-section pb-0" id="parceiros">
            <div class="container relative">

                <h2 class="section-title font-alt mb-70 mb-sm-40">
                    Clientes & Parceiros
                </h2>

                <div class="linha-parceiro">
                    <div class="col-sm-3 wow fadeInUp align-center logo-parceiro">
                        <img src="images/clientes/ambev.png" alt="" />
                    </div>
                    <div class="col-sm-3 wow fadeInUp align-center logo-parceiro">
                        <img src="images/clientes/danone.png" alt="" />
                    </div>
                    <div class="col-sm-3 wow fadeInUp align-center logo-parceiro">
                        <img src="images/clientes/kraft-heinz.png" alt="" />
                    </div>
                    <div class="col-sm-3 wow fadeInUp align-center logo-parceiro">
                        <img src="images/clientes/flora.png" alt="" />
                    </div>
                </div>

                <div class="linha-parceiro">
                    <div class="col-sm-4 wow fadeInUp align-center logo-parceiro">
                        <img src="images/clientes/backus.png" alt="" />
                    </div>
                    <div class="col-sm-4 wow fadeInUp align-center logo-parceiro">
                        <img src="images/clientes/johnson.png" alt="" />
                    </div>
                    <div class="col-sm-4 wow fadeInUp align-center logo-parceiro">
                        <img src="images/clientes/pepsi.png" alt="" />
                    </div>
                </div>

                <div class="linha-parceiro">
                    <div class="col-sm-2 wow fadeInUp align-center logo-parceiro">
                        <img src="images/clientes/ball.png" alt="" />
                    </div>
                    <div class="col-sm-3 wow fadeInUp align-center logo-parceiro">
                        <img src="images/clientes/exal.png" alt="" />
                    </div>
                    <div class="col-sm-3 wow fadeInUp align-center logo-parceiro">
                        <img src="images/clientes/crown.png" alt="" />
                    </div>
                    <div class="col-sm-4 wow fadeInUp align-center logo-parceiro">
                        <img src="images/clientes/ardagh.png" alt="" />
                    </div>
                </div>

            </div>
        </section>
        <!-- Clientes e Parceiros Section -->

@stop
