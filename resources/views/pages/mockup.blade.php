@extends('common.template')

@section('title')
	Mock-up
@stop

@section('content')

		<link type="text/css" rel="stylesheet" href="css/angle.css" />
        <script type="text/javascript" src="js/jquery.angle.js"></script>
		<script type="text/javascript">
            $(document).ready(function() {
                $('#angle-view1').angle({
                    speed: 3,
                    drag: true
                });
                $('#angle-view2').angle({
                    speed: 3,
                    drag: true
                });
            });
        </script>

	    <!-- Head Section -->
	    <section class="page-section bg-dark-alfa-30 parallax-3" data-background="images/full-width-images/mock-up-full-landing.jpg">
	        <div class="relative container align-left">
	            <div class="row">
	                <div class="col-md-8">
	                    <h1 class="hs-line-11 font-alt mb-20 mb-xs-0">Mock-up</h1>
	                    <div class="hs-line-4 font-alt">
	                        TIRANDO A IDEIA DO PAPEL
	                    </div>
	                </div>
	            </div>
	        </div>
	    </section>
	    <!-- End Head Section -->

	    <!-- Text Section -->
	    <section class="page-section small-section" id="about">
	        <div class="container relative">
	            <div class="row">
	                <div class="col-md-10 col-md-offset-1">
	                    <div class="section-text align-center">
	                        <blockquote>
	                            <p>
	                                Mock-up é solução ideal para dar vida a uma ideia, um projeto. Possui inúmeras utilidades no mercado de embalagem,
									seja nas mãos dos gestores de produtos, agências ou de clientes. A PaintPack produz mock-ups para qualquer tipo de
									embalagem física ou em 3D. Os mock-ups virtuais em 3D são interativos, podem ser girados em 360° para avaliar a
									embalagem por completo.
	                            </p>
	                        </blockquote>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </section>
	    <!-- End Text Section -->

	    <!-- Divider -->
	    <hr class="mt-0 mb-0 "/>
	    <!-- End Divider -->

	    <!-- Aplicações e Vantagens Section -->
	    <section class="page-section small-section" id="start">
	        <div class="container relative">
	            <!-- Section Headings -->
	            <div class="row">
	                <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 align-center">
	                    <h3 class="font-alt mb-70 mb-sm-40">APLICAÇÕES E VANTAGENS</h3>
	                    <div class="section-text mb-80 mb-xs-40">
	                        Transformar a ideia em realidade através de um mock-up é a única
							forma de avaliar com precisão o aplicabilidade de um projeto.
	                    </div>
	                </div>
	            </div>
	            <!-- End Section Headings -->

	            <!-- Service Grid -->
	            <div class="alt-service-grid">
	                <div class="row">
	                    <div class="col-sm-6 col-md-3 col-lg-3 no-padding">
	                        <!-- Alt Service Item -->
							<div class="alt-service-wrap">
				                <div class="alt-service-item mockup-service-item">
				                    <div class="alt-features-icon align-center">
				                        <span class="icon-presentation"></span>
				                    </div>
				                    <h3 class="alt-features-title font-alt align-center">Pesquisa de Mercado</h3>
				                    <div class="alt-features-descr align-left">
				                        Mock-ups físicos são armas poderosas em pesquisas quantitativa e qualitativa.
										Com eles o consumidor pode avaliar com precisão uma nova identidade visual para
										um produto já consolidado no mercado ou um novo produto a ser lançado.
				                    </div>
				                </div>
				            </div>
	                        <!-- End Service Item -->

	                        <!-- Alt Service Item -->
							<div class="alt-service-wrap">
				                <div class="alt-service-item mockup-service-item">
				                    <div class="alt-features-icon align-center">
				                        <span class="icon-video"></span>
				                    </div>
				                    <h3 class="alt-features-title font-alt align-center">Filmes Comerciais</h3>
				                    <div class="alt-features-descr align-left">
				                        Produzimos mock-ups de qualidade excepcional com foco em reduzir o trabalho da
										produtora e pensando nas necessidades do filme. Se o um take do comericial exige
										que o ator abra uma lata e consuma a bebida, preparamos o mock-up com o logo posicionado
										de forma a priorizar o enquadramento da marca na cena. São detalhes como esse que fazem
										da PaintPack a líder no segmento de mock-ups físicos da América do Sul.
				                    </div>
				                </div>
				            </div>
	                        <!-- End Service Item -->
	                    </div>
	                    <div class="col-md-6 col-lg-6 hidden-xs hidden-sm">
	                        <div class="alt-services-image mockup-service-image">
	                            <img src="images/front-end/full-width-images/mock-up-ball.png" alt="" />
	                        </div>
	                    </div>
	                    <div class="col-sm-6 col-md-3 col-lg-3 no-padding">
	                        <!-- Alt Service Item -->
							<div class="alt-service-wrap">
				                <div class="alt-service-item mockup-service-item">
				                    <div class="alt-features-icon align-center">
				                        <span class="icon-camera"></span>
				                    </div>
				                    <h3 class="alt-features-title font-alt align-center">Fotografia</h3>
				                    <div class="alt-features-descr align-left">
				                        Atenção aos detalhes são fundamentais para os mock-ups destinados a fotografia.
										Como fazer o mock-up perfeito para o fotógrafo captar a beleza real da embalagem?
										Essa pergunta direciona nossos esforços para criar soluções como a aplicação de
										vernizes especiais para reduzir reflexos. Um mock-up 3D eventualmente pode substituir
										a necessidade de uma fotografia, a PaintPack dispõe de softwares de ponta para produzir
										os melhores resultados.
				                    </div>
				                </div>
				            </div>
	                        <!-- End Service Item -->

	                        <!-- Alt Service Item -->
							<div class="alt-service-wrap">
				                <div class="alt-service-item mockup-service-item">
				                    <div class="alt-features-icon align-center">
				                        <span class="icon-hotairballoon"></span>
				                    </div>
				                    <h3 class="alt-features-title font-alt align-center">Avaliação de Layout</h3>
				                    <div class="alt-features-descr align-left">
				                        Cores e elementos gráficos se comportam de forma diferente dependendo do método de impressão
										que a embalagem necessita. Uma arte criada só pode ser avaliada com precisão através de um mock-up.
										Somos especialistas nos mais diversos processos de impressão: offset, flexografia, rotogravura,
										silk-screen e dry offset, e simulamos em nossos mock-ups as características reais da produção
										industrial nesses processos.
				                    </div>
				                </div>
				            </div>
	                        <!-- End Service Item -->
	                    </div>
	                </div>
	            </div>
	            <!-- End Service Grid -->
	        </div>
	    </section>
	    <!-- End Alternative Services Section -->

	    <!-- Divider -->
	    <hr class="mt-0 mb-0 "/>
	    <!-- End Divider -->

		<!-- Fizico x Virtual Section -->
	    <section class="page-section small-section" id="start">
	        <div class="container relative">
	            <!-- Section Headings -->
	            <div class="row">
	                <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 align-center">
	                    <h3 class="font-alt mb-70 mb-sm-40">Mock-up Físico x Virtual</h3>
	                    <div class="section-text mb-80 mb-xs-40">
	                        Seja pela praticidade ou pela necessidade de otimizar custos de um projeto, os mock-ups 3D
							estão cada vez mais presentes e demandados. A qualidade e realismo das imagens geradas,
							aliadas à interatividade desses modelos virtuais que podem ser girados em 360°, muitas vezes
							substitui a necessidade de um mock-up físico.
	                    </div>
	                </div>
					<div class="col-xs-12 col-md-12 col-lg-12 align-center">
						<img src="images/front-end/full-width-images/mock-up-clique-arraste.png" alt="" />
					</div>
	            </div>
	            <!-- End Section Headings -->

	            <!-- Service Grid -->
	            <div class="alt-service-grid">
	                <div class="">
	                    <div class="col-xs-8 col-md-2 no-padding">

	                    </div>
	                    <div class="col-xs-8 col-xs-offset-2 col-md-4 col-md-offset-0">
							<div class="alt-services-image angle-view" id="angle-view1">
								{{-- <img src="images/front-end/full-width-images/mock-up-fisico.png" alt="" /> --}}
								<ul>
									<li><img src="images/mockup360/01.jpg" alt="01" /></li>
									<li><img src="images/mockup360/03.jpg" alt="03" /></li>
									<li><img src="images/mockup360/05.jpg" alt="05" /></li>
									<li><img src="images/mockup360/07.jpg" alt="07" /></li>
									<li><img src="images/mockup360/09.jpg" alt="09" /></li>
									<li><img src="images/mockup360/11.jpg" alt="11" /></li>
									<li><img src="images/mockup360/13.jpg" alt="13" /></li>
									<li><img src="images/mockup360/15.jpg" alt="15" /></li>
								</ul>
							</div>
							<h3 class="alt-features-title font-alt align-center">MOCK-UP FÍSICO</h3>
							<div class="alt-features-descr align-left">
								Utilizamos impressoras e tintas de tecnologia avançada que aliadas aos substratos de alta qualidade,
								simulam fielmente o resultado que será obtido na produção industrial.
							</div>
	                    </div>
						<div class="col-xs-8 col-xs-offset-2 col-md-4 col-md-offset-0">
							<div class="alt-services-image angle-view" id="angle-view2">
								{{-- <img src="images/front-end/full-width-images/mock-up-3d.png" alt="" /> --}}
								<ul>
									<li><img src="images/mockup360/01.jpg" alt="01" /></li>
									<li><img src="images/mockup360/02.jpg" alt="02" /></li>
									<li><img src="images/mockup360/03.jpg" alt="03" /></li>
									<li><img src="images/mockup360/04.jpg" alt="04" /></li>
									<li><img src="images/mockup360/05.jpg" alt="05" /></li>
									<li><img src="images/mockup360/06.jpg" alt="06" /></li>
									<li><img src="images/mockup360/07.jpg" alt="07" /></li>
									<li><img src="images/mockup360/08.jpg" alt="08" /></li>
									<li><img src="images/mockup360/09.jpg" alt="09" /></li>
									<li><img src="images/mockup360/10.jpg" alt="10" /></li>
									<li><img src="images/mockup360/11.jpg" alt="11" /></li>
									<li><img src="images/mockup360/12.jpg" alt="12" /></li>
									<li><img src="images/mockup360/13.jpg" alt="13" /></li>
									<li><img src="images/mockup360/14.jpg" alt="14" /></li>
									<li><img src="images/mockup360/15.jpg" alt="15" /></li>
									<li><img src="images/mockup360/16.jpg" alt="16" /></li>
								</ul>
							</div>
							<h3 class="alt-features-title font-alt align-center">MOCK-UP 3D</h3>
							<div class="alt-features-descr align-left">
								Os modelos virtuais em 3D que criamos possuem medidas, texturas e cores tão reais que fica difícil
								distingui-los de uma foto. Temos um vasto catálogo de modelos de embalagens TetraPak, além de latas,
								packs, shrinks dentre outros tipos.
							</div>
	                    </div>
	                    <div class="col-xs-8 col-md-2 no-padding">

	                    </div>
	                </div>
	            </div>
	            <!-- End Service Grid -->
	        </div>
	    </section>
	    <!-- End Alternative Services Section -->

		<!-- Divider -->
        <hr class="mt-0 mb-0" style="padding: 120px 0;"/>
        <!-- End Divider -->

		<section class="page-section pt-0 pb-0 banner-section bg-dark" data-background="#000000">
            <div class="container relative">
                <div class="row">
                    <div class="col-sm-7">
                        <div class="mt-140 mt-lg-80 mb-140 mb-lg-80">
                            <div class="banner-content">
                                <h3 class="banner-heading font-alt">Paintpack RED</h3>
                                <div class="banner-decription">
                                    - Todos os seus produtos e SKUs organizados em um só lugar<br>
									- Repositório de imagens em alta resolução na nuvem<br>
									- Gerenciamento das artes-finais em sincronia com as últimas versões
                                </div>
                                <div class="local-scroll">
                                    <a href="/red" class="btn btn-mod btn-w btn-medium btn-round">Conheça</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 hidden-xs hidden-sm banner-image-mockup wow fadeInUp">
                        <img src="images/front-end/full-width-images/mock-up-shelf-red.png" alt="" />
                    </div>
                </div>
            </div>
        </section>
	</div>
	<!-- End Page Wrap -->

@stop
