@extends('common.template')

@section('title')
	Sobre
@stop

@section('content')

	    <!-- Head Section -->
	    <section class="page-section bg-dark-alfa-30 parallax-3" data-background="images/full-width-images/dry-offset-full-landing.jpg">
	        <div class="relative container align-left">

	            <div class="row">

	                <div class="col-md-8">
	                    <h1 class="hs-line-11 font-alt mb-20 mb-xs-0">Landing Page</h1>
	                    <div class="hs-line-4 font-alt">
	                        Lorem ipsum dolor sit amet, consectetur adipiscing
	                    </div>
	                </div>
	            </div>

	        </div>
	    </section>
	    <!-- End Head Section -->


	    <!-- Text Section -->
	    <section class="page-section" id="about">
	        <div class="container relative">

	            <div class="row">
	                <div class="col-md-8 col-md-offset-2">
	                    <div class="section-text align-center">
	                        <blockquote>
	                            <p>
	                                Curabitur eu adipiscing lacus, a iaculis diam. Nullam placerat blandit auctor.
	                                Nulla accumsan ipsum et nibh rhoncus, eget tempus sapien ultricies. Donec mollis
	                                lorem vehicula.
	                            </p>
	                        </blockquote>

	                        <div class="local-scroll">
	                            <a href="#start" class="btn btn-mod btn-border btn-medium btn-round mb-10">See More</a>
	                            <span class="hidden-xs">&nbsp;</span>
	                            <a href="http://themeforest.net/user/theme-guru/portfolio" class="btn btn-mod btn-medium btn-round mb-10">Buy Now</a>
	                        </div>

	                    </div>
	                </div>
	            </div>

	        </div>
	    </section>
	    <!-- End Text Section -->


	    <!-- Divider -->
	    <hr class="mt-0 mb-0 "/>
	    <!-- End Divider -->


	    <!-- Alternative Services Section -->
	    <section class="page-section" id="start">
	        <div class="container relative">

	            <!-- Section Headings -->
	            <div class="row">
	                <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 align-center">

	                    <h3 class="font-alt mb-70 mb-sm-40">We provide best digital services</h3>

	                    <div class="section-text mb-80 mb-xs-40">
	                        Curabitur eu adipiscing lacus, a iaculis diam. Nullam placerat blandit auctor. Nulla accumsan ipsum et nibh rhoncus, eget tempus sapien ultricies. Donec mollis lorem vehicula.
	                    </div>

	                </div>
	            </div>
	            <!-- End Section Headings -->

	            <!-- Service Grid -->
	            <div class="alt-service-grid">
	                <div class="row">


	                    <div class="col-sm-6 col-md-3 col-lg-3">

	                        <!-- Alt Service Item -->
	                        <div class="alt-service-wrap">
	                            <div class="alt-service-item">
	                                <div class="alt-service-icon">
	                                    <i class="fa fa-gift"></i>
	                                </div>
	                                <h3 class="alt-services-title font-alt">Branding</h3>
	                                Lorem ipsum dolor sit amet,  elit. Vitae gravida nibh. Morbi dignissim nunc at risus convallis.
	                            </div>

	                        </div>
	                        <!-- End Service Item -->

	                        <!-- Alt Service Item -->
	                        <div class="alt-service-wrap">
	                            <div class="alt-service-item">
	                                <div class="alt-service-icon">
	                                    <i class="fa fa-desktop"></i>
	                                </div>
	                                <h3 class="alt-services-title font-alt">Web Design</h3>
	                                Vivamus neque orci, ultricies blandit ultricies vel, semper interdum elit, non placerat suscipit.
	                            </div>


	                        </div>
	                        <!-- End Service Item -->

	                        <!-- Alt Service Item -->
	                        <div class="alt-service-wrap">
	                            <div class="alt-service-item">
	                                <div class="alt-service-icon">
	                                    <i class="fa fa-bug"></i>
	                                </div>
	                                <h3 class="alt-services-title font-alt">Logo Design</h3>
	                                Lorem ipsum dolor sit amet,  elit. Vitae gravida nibh. Morbi dignissim nunc at risus convallis.
	                            </div>


	                        </div>
	                        <!-- End Service Item -->

	                        <!-- Alt Service Item -->
	                        <div class="alt-service-wrap">
	                            <div class="alt-service-item">
	                                <div class="alt-service-icon">
	                                    <i class="fa fa-heart"></i>
	                                </div>
	                                <h3 class="alt-services-title font-alt">Illustrations</h3>
	                                Semper interdum ultricies elit, non placerat nisi suscipit nunc at risus convallis.
	                            </div>

	                        </div>
	                        <!-- End Service Item -->

	                    </div>


	                    <div class="col-md-6 col-lg-6 hidden-xs hidden-sm">

	                        <div class="alt-services-image">
	                            <img src="images/promo-2.png" alt="" />
	                        </div>

	                    </div>


	                    <div class="col-sm-6 col-md-3 col-lg-3">

	                        <!-- Alt Service Item -->
	                        <div class="alt-service-wrap">
	                            <div class="alt-service-item">
	                                <div class="alt-service-icon">
	                                    <i class="fa fa-cogs"></i>
	                                </div>
	                                <h3 class="alt-services-title font-alt">Development</h3>
	                                Morbi dignissim nunc at risus convallis. Semper interdum ultricies elit, non placerat nisi suscipit.
	                            </div>

	                        </div>
	                        <!-- End Service Item -->

	                        <!-- Alt Service Item -->
	                        <div class="alt-service-wrap">
	                            <div class="alt-service-item">
	                                <div class="alt-service-icon">
	                                    <i class="fa fa-camera"></i>
	                                </div>
	                                <h3 class="alt-services-title font-alt">Photography</h3>
	                                Lorem ipsum dolor sit amet,  elit. Vitae gravida nibh. Morbi dignissim nunc at risus convallis.
	                            </div>


	                        </div>
	                        <!-- End Service Item -->

	                        <!-- Alt Service Item -->
	                        <div class="alt-service-wrap">
	                            <div class="alt-service-item">
	                                <div class="alt-service-icon">
	                                    <i class="fa fa-area-chart"></i>
	                                </div>
	                                <h3 class="alt-services-title font-alt">Marketing</h3>
	                                Neque orci, ultricies blandit ultricies vel, semper interdum ultricies elit, non placerat nisi suscipit.
	                            </div>


	                        </div>
	                        <!-- End Service Item -->

	                        <!-- Alt Service Item -->
	                        <div class="alt-service-wrap">
	                            <div class="alt-service-item">
	                                <div class="alt-service-icon">
	                                    <i class="fa fa-life-ring"></i>
	                                </div>
	                                <h3 class="alt-services-title font-alt">Support</h3>
	                                Lorem ipsum dolor sit amet,  elit. Vitae gravida nibh dignissim nunc risus convallis.
	                            </div>


	                        </div>
	                        <!-- End Service Item -->

	                    </div>


	                </div>
	            </div>
	            <!-- End Service Grid -->

	        </div>
	    </section>
	    <!-- End Alternative Services Section -->


	    <!-- Divider -->
	    <hr class="mt-0 mb-0 "/>
	    <!-- End Divider -->


	    <!-- Section -->
	    <section class="page-section">
	        <div class="container relative">

	            <div class="row">

	                <div class="col-md-7 mb-sm-40">

	                    <!-- Gallery -->
	                    <div class="work-full-media mt-0 white-shadow">
	                        <ul class="clearlist work-full-slider owl-carousel">
	                            <li>
	                                <img src="images/promo-3.png" alt="" />
	                            </li>
	                            <li>
	                                <img src="images/portfolio/full-project-2.jpg" alt="" />
	                            </li>
	                        </ul>
	                    </div>
	                    <!-- End Gallery -->

	                </div>

	                <div class="col-md-5 col-lg-4 col-lg-offset-1">

	                    <!-- About Project -->
	                    <div class="text">

	                        <h3 class="font-alt mb-30 mb-xxs-10">Creative Project</h3>

	                        <p>
	                            Phasellus facilisis mauris vel velit molestie pellentesque. Donec rutrum, tortor ut elementum venenatis, purus magna bibendum nisl, ut accumsan ipsum erat eu sapien.
	                        </p>

	                        <div class="mt-40">
	                            <a href="#" class="btn btn-mod btn-border btn-round btn-medium" target="_blank">View online</a>
	                        </div>

	                    </div>
	                    <!-- End About Project -->

	                </div>
	            </div>
	        </div>
	    </section>
	    <!-- End Section -->


	    <!-- Divider -->
	    <hr class="mt-0 mb-0 "/>
	    <!-- End Divider -->


	    <!-- Section -->
	    <section class="page-section">
	        <div class="container relative">

	            <div class="row">

	                <div class="col-md-5 col-lg-4 mb-sm-40">

	                    <!-- About Project -->
	                    <div class="text">

	                        <h3 class="font-alt mb-30 mb-xxs-10">Creative Project</h3>
	                        <p>
	                            Phasellus facilisis mauris vel velit molestie pellentesque. Donec rutrum, tortor ut elementum venenatis, purus magna bibendum nisl, ut accumsan ipsum erat eu sapien.
	                        </p>

	                        <div class="mt-40">
	                            <a href="#" class="btn btn-mod btn-border btn-round btn-medium" target="_blank">View online</a>
	                        </div>

	                    </div>
	                    <!-- End About Project -->

	                </div>

	                <div class="col-md-7 col-lg-offset-1">

	                    <!-- Work Gallery -->
	                    <div class="work-full-media mt-0 white-shadow">
	                        <ul class="clearlist work-full-slider owl-carousel">
	                            <li>
	                                <img src="images/promo-4.png" alt="" />
	                            </li>
	                            <li>
	                                <img src="images/portfolio/full-project-4.jpg" alt="" />
	                            </li>
	                        </ul>
	                    </div>
	                    <!-- End Work Gallery -->

	                </div>

	             </div>

	        </div>
	    </section>
	    <!-- End Section -->


	    <!-- Divider -->
	    <hr class="mt-0 mb-0 "/>
	    <!-- End Divider -->


	    <!-- Features Section -->
	    <section class="page-section">
	        <div class="container relative">

	            <h2 class="section-title font-alt mb-70 mb-sm-40">
	                Why Choose Us?
	            </h2>

	            <!-- Features Grid -->
	            <div class="row multi-columns-row alt-features-grid">

	                <!-- Features Item -->
	                <div class="col-sm-6 col-md-4 col-lg-4">
	                    <div class="alt-features-item align-center">
	                        <div class="alt-features-icon">
	                            <span class="icon-flag"></span>
	                        </div>
	                        <h3 class="alt-features-title font-alt">We’re Creative</h3>
	                        <div class="alt-features-descr align-left">
	                            Lorem ipsum dolor sit amet, c-r adipiscing elit.
	                            In maximus ligula semper metus pellentesque mattis.
	                            Maecenas  volutpat, diam enim.
	                        </div>
	                    </div>
	                </div>
	                <!-- End Features Item -->

	                <!-- Features Item -->
	                <div class="col-sm-6 col-md-4 col-lg-4">
	                    <div class="alt-features-item align-center">
	                        <div class="alt-features-icon">
	                            <span class="icon-clock"></span>
	                        </div>
	                        <h3 class="alt-features-title font-alt">We’re Punctual</h3>
	                        <div class="alt-features-descr align-left">
	                            Proin fringilla augue at maximus vestibulum.
	                            Nam pulvinar vitae neque et porttitor. Praesent sed
	                            nisi eleifend, lorem fermentum orci sit amet, iaculis libero.
	                        </div>
	                    </div>
	                </div>
	                <!-- End Features Item -->

	                <!-- Features Item -->
	                <div class="col-sm-6 col-md-4 col-lg-4">
	                    <div class="alt-features-item align-center">
	                        <div class="alt-features-icon">
	                            <span class="icon-hotairballoon"></span>
	                        </div>
	                        <h3 class="alt-features-title font-alt">We have magic</h3>
	                        <div class="alt-features-descr align-left">
	                            Curabitur iaculis accumsan augue, nec finibus mauris pretium eu.
	                            Duis placerat ex gravida nibh tristique porta. Nulla facilisi.
	                            Suspendisse ultricies eros blandit.
	                        </div>
	                    </div>
	                </div>
	                <!-- End Features Item -->

	            </div>
	            <!-- End Features Grid -->

	        </div>
	    </section>
	    <!-- End Features Section -->


	    <!-- Call Action Section -->
	    <section class="page-section pt-0 pb-0 banner-section bg-dark" data-background="images/full-width-images/section-bg-2.jpg">
	        <div class="container relative">

	            <div class="row">

	                <div class="col-sm-6">

	                    <div class="mt-140 mt-lg-80 mb-140 mb-lg-80">
	                        <div class="banner-content">
	                            <h3 class="banner-heading font-alt">Looking for exclusive digital services?</h3>
	                            <div class="banner-decription">
	                                Proin fringilla augue at maximus vestibulum. Nam pulvinar vitae neque et porttitor.
	                                Integer non dapibus diam, ac eleifend lectus.
	                            </div>
	                            <div class="local-scroll">
	                                <a href="#contact" class="btn btn-mod btn-w btn-medium btn-round">Lets talk</a>
	                            </div>
	                        </div>
	                    </div>

	                </div>

	                <div class="col-sm-6 banner-image wow fadeInUp">
	                    <img src="images/promo-1.png" alt="" />
	                </div>

	            </div>

	        </div>
	    </section>
	    <!-- End Call Action Section -->


	    <!-- Foter -->
	    <footer class="page-section bg-gray-lighter footer pb-60">
	        <div class="container">

	            <!-- Footer Logo -->
	            <div class="local-scroll mb-30 wow fadeInUp" data-wow-duration="1.5s">
	                <a href="#top"><img src="images/logo-footer.png" width="78" height="36" alt="" /></a>
	            </div>
	            <!-- End Footer Logo -->

	            <!-- Social Links -->
	            <div class="footer-social-links mb-110 mb-xs-60">
	                <a href="#" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
	                <a href="#" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>
	                <a href="#" title="Behance" target="_blank"><i class="fa fa-behance"></i></a>
	                <a href="#" title="LinkedIn+" target="_blank"><i class="fa fa-linkedin"></i></a>
	                <a href="#" title="Pinterest" target="_blank"><i class="fa fa-pinterest"></i></a>
	            </div>
	            <!-- End Social Links -->

	            <!-- Footer Text -->
	            <div class="footer-text">

	                <!-- Copyright -->
	                <div class="footer-copy font-alt">
	                    <a href="http://themeforest.net/user/theme-guru/portfolio" target="_blank">&copy; Rhythm 2016</a>.
	                </div>
	                <!-- End Copyright -->

	                <div class="footer-made">
	                    Made with love for great people.
	                </div>

	            </div>
	            <!-- End Footer Text -->

	         </div>


	         <!-- Top Link -->
	         <div class="local-scroll">
	             <a href="#top" class="link-to-top"><i class="fa fa-caret-up"></i></a>
	         </div>
	         <!-- End Top Link -->

	    </footer>
	    <!-- End Foter -->


	</div>
	<!-- End Page Wrap -->

@stop
