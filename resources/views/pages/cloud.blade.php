<!DOCTYPE html>
<html>
    <head>
        <title>PaintPack - Cloud</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta name="author" content="Fernando Maio">
        <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <!-- Favicons -->
        <link rel="shortcut icon" href="images/favicon.png">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">

        <!-- CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/style-responsive.css">
        <link rel="stylesheet" href="css/animate.min.css">
        <link rel="stylesheet" href="css/vertical-rhythm.min.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/magnific-popup.css">

    </head>
    <body class="appear-animate">
        <!-- Page Loader -->
        <div class="page-loader">
            <div class="loader">Loading...</div>
        </div>
        <!-- End Page Loader -->
        <div class="page" id="top">
        <!-- Head Section -->
        <section class="page-section bg-dark-alfa-30" style="background-position: 60% 0px; background-attachment: initial;"
            data-background="images/front-end/full-width-images/cloud-banner.jpg">
            <div class="relative container align-left">
                <div class="row">
                    <div class="col-md-8">
                        <h1 class="hs-line-11 font-alt mb-20 mb-xs-0">PAINTPACK CLOUD</h1>
                        <div class="hs-line-4 font-alt">
                            ARTES ORGANIZADAS, ATUALIZADAS E SEMPRE DISPONÍVEIS
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Head Section -->

        <!-- Text Section -->
        <section class="page-section small-section" id="about">
            <div class="container relative">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="section-text align-center">
                            <blockquote>
                                <p>
                                    Apresentamos o fluxo inteligente de artes com o PaintPack Cloud.
                                    A conexão entre marcas, agências e fornecedores fica sincronizada
                                    e os arquivos sempre atualizados e seguros
                                    com backup na nuvem.
                                </p>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Text Section -->

        <!-- Divider -->
        <hr class="mt-0 mb-0 "/>
        <!-- End Divider -->

        <section class="page-section small-section" id="start">
            <div class="container relative">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 align-center">
                        <h3 class="font-alt mb-10 mb-sm-40">PRINCIPAIS FUNÇÕES</h3>
                    </div>
                </div>
                <div class="alt-service-grid">
                    <div class="">
                        <div class="col-sm-6 col-md-2 col-lg-2 cloud-landing">
                            <div class="alt-service-wrap mb-40">
                                <div class="alt-service-item cloud-landing">
                                    <h3 class="alt-services-title font-alt">CAMPO DE BUSCA</h3>
                                    Digite o nome do arquivo ou pasta que deseja ter acesso.
                                </div>
                            </div>
                            <div class="alt-service-wrap mb-40">
                                <div class="alt-service-item cloud-landing">
                                    <h3 class="alt-services-title font-alt">PASTAS</h3>
                                    A organização do PaintPack Cloud é igual às pastas e arquivos no computador.
                                    Para abrir uma pasta dê um clique e então navegue pelas pastas subsequentes até
                                    chegar ao arquivo desejado, que pode ser visualizado, baixado ou compartilhado.
                                </div>
                            </div>
                            <div class="alt-service-wrap mb-40">
                                <div class="alt-service-item cloud-landing">
                                    <h3 class="alt-services-title font-alt">FAVORITOS</h3>
                                    Localizado na barra lateral existe a opção Favoritos. Para marcar suas pastas mais
                                    importantes, basta clicar na estrela ao lado do nome da pasta e depois acessá-las pelo atalho.
                                </div>
                            </div>
                        </div>

                        <div class="col-md-8 col-lg-8 hidden-xs hidden-sm">
                            <div class="alt-services-image">
                                <img src="images/front-end/full-width-images/cloud-browser.jpg" alt="" style="margin-top: 100px;" />
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-2 col-lg-2 cloud-landing">
                            <div class="alt-service-wrap mb-40">
                                <div class="alt-service-item cloud-landing">
                                    <h3 class="alt-services-title font-alt">USUÁRIO</h3>
                                    Visualize as informações da sua conta aqui. Também é o local de sair do sistema.
                                </div>
                            </div>
                            <div class="alt-service-wrap mb-40">
                                <div class="alt-service-item cloud-landing">
                                    <h3 class="alt-services-title font-alt">COMPARTILHAR</h3>
                                    Para compartilhar uma pasta com outra pessoa, basta clicar aqui e obter o link compartilhado.
                                    Você pode colar esse link e enviar da forma que preferir, ou então digitar o e-mail do destinatário
                                    que o sistema faz isso por você.
                                </div>
                            </div>
                            <div class="alt-service-wrap mb-40">
                                <div class="alt-service-item cloud-landing">
                                    <h3 class="alt-services-title font-alt">INFORMAÇÕES</h3>
                                    Dados como data de modificação e quantidade de arquivos são exibidas nessas colunas. O símbolo azul
                                    na frente da pasta, marca que esta já possui um link compartilhado.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="page-section small-section">
            <div class="container relative">
                <h2 class="section-title font-alt mb-50 mb-sm-40">NAVEGUE ENTRE AS PASTAS</h2>
                <div class="row multi-columns-row alt-features-grid">
                    <!-- Features Item -->
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="alt-features-item align-center">
                            <div class="">
                                <img src="images/front-end/full-width-images/cloud-navegar1.png" />
                            </div>
                            <div class="alt-features-descr alt-features-cloud align-left">
                                Para navegar entre as pastas do PaintPack Cloud, basta selecionar a marca
                                do produto desejado e clicar uma vez.
                            </div>
                        </div>
                    </div>
                    <!-- End Features Item -->

                    <!-- Features Item -->
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="alt-features-item align-center">
                            <div class="">
                                <img src="images/front-end/full-width-images/cloud-navegar2.png" />
                            </div>
                            <div class="alt-features-descr alt-features-cloud align-left">
                                Entre a lista de pastas, clique na do produto desejado e na sequência selecione
                                as pastas seguintes de acordo com o tipo de embalagem e volume.
                            </div>
                        </div>
                    </div>
                    <!-- End Features Item -->

                    <!-- Features Item -->
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="alt-features-item align-center">
                            <div class="">
                                <img src="images/front-end/full-width-images/cloud-navegar3.png" />
                            </div>
                            <div class="alt-features-descr alt-features-cloud align-left">
                                Uma vez dentro da pasta do SKU, você terá acesso aos arquivos PDF para visualização
                                das artes-finais, bem como arquivos ZIP em alta-resolução.
                            </div>
                        </div>
                    </div>
                    <!-- End Features Item -->
                </div>
            </div>
        </section>

        <section class="page-section small-section">
            <div class="container relative">
                <h2 class="section-title font-alt mb-50 mb-sm-40">VISUALIZE OS ARQUIVOS</h2>
                <div class="row multi-columns-row alt-features-grid">
                    <!-- Features Item -->
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="alt-features-item align-center">
                            <div class="">
                                <img src="images/front-end/full-width-images/cloud-visualize1.png" />
                            </div>
                            <div class="alt-features-descr alt-features-cloud align-left">
                                Dentro da pasta do SKU, selecione o arquivo PDF desejado, clique e aguarde a visualização
                                ser carregada. Isso pode levar alguns instantes dependendo da velocidade da sua conexão.
                            </div>
                        </div>
                    </div>
                    <!-- End Features Item -->

                    <!-- Features Item -->
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="alt-features-item align-center">
                            <div class="">
                                <img src="images/front-end/full-width-images/cloud-visualize2.png" />
                            </div>
                            <div class="alt-features-descr alt-features-cloud align-left">
                                Uma vez a imagem carregada, você pode ampliá-la para ver mais detalhes, clique nas
                                lupas para aumentar ou reduzir. Para exibir a imagem em tela cheia, basta clicar no
                                ícone representado por 4 setas em expansão. Existem arquivos com mais de uma página,
                                atente para isso.
                            </div>
                        </div>
                    </div>
                    <!-- End Features Item -->

                    <!-- Features Item -->
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="alt-features-item align-center">
                            <div class="">
                                <img src="images/front-end/full-width-images/cloud-visualize3.png" />
                            </div>
                            <div class="alt-features-descr alt-features-cloud align-left">
                                O PaintPack Cloud é capaz de exibir visualização para diversas extensões de arquivo,
                                como: PDF, TIFF, RTF e DOC. Porém arquivos ZIP não possuem visualização. Para ter acesso
                                ao conteúdo compactado, é necessário baixar o ZIP e abrir localmente.
                            </div>
                        </div>
                    </div>
                    <!-- End Features Item -->
                </div>
            </div>
        </section>

        <section class="page-section small-section">
            <div class="container relative">
                <h2 class="section-title font-alt mb-50 mb-sm-40">BAIXE E COMPARTILHE</h2>
                <div class="row multi-columns-row alt-features-grid">
                    <!-- Features Item -->
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="alt-features-item align-center">
                            <div class="">
                                <img src="images/front-end/full-width-images/cloud-baixe1.png" />
                            </div>
                            <div class="alt-features-descr alt-features-cloud align-left">
                                Após abrir o arquivo para visualização, é possível baixá-lo para arquivamento local no
                                seu computador. Para isso, clique no botão “baixar” localizado no canto superior direito
                                da tela. O peso do arquivo será exibido, assim é possível estimar o tempo levado para baixar.
                            </div>
                        </div>
                    </div>
                    <!-- End Features Item -->

                    <!-- Features Item -->
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="alt-features-item align-center">
                            <div class="">
                                <img src="images/front-end/full-width-images/cloud-baixe2.png" />
                            </div>
                            <div class="alt-features-descr alt-features-cloud align-left">
                                Uma pasta pode ser compartilhada com outras pessoas através de um link compartilhado,
                                mesmo que estas não sejam usuários do PaintPack Cloud. Para isso, clique no botão
                                “Compartilhar esta pasta” e sem seguida “Obter link compartilhado”.
                            </div>
                        </div>
                    </div>
                    <!-- End Features Item -->

                    <!-- Features Item -->
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="alt-features-item align-center">
                            <div class="">
                                <img src="images/front-end/full-width-images/cloud-baixe3.png" />
                            </div>
                            <div class="alt-features-descr alt-features-cloud align-left">
                                Assim o sistema irá exibir uma janela com o link pronto para se copiado e colado.
                                Você pode enviar esse link por e-mail, ou apenas digitar o endereço do destinatário
                                no campo para que o sistema faça o envio. Todos os links possuem data de expiração,
                                atente para isso.
                            </div>
                        </div>
                    </div>
                    <!-- End Features Item -->
                </div>
            </div>
        </section>

        <!-- Foter -->
        <footer class="page-section bg-gray-lighter footer pb-60">
            <div class="container">
                <!-- Footer Logo -->
                <div class="local-scroll mb-30 wow fadeInUp" data-wow-duration="1.5s">
                    <a href="#top"><img src="images/front-end/logo-footer.png" alt="Logo Rodapé" /></a>
                </div>
                <!-- End Footer Logo -->
                <!-- Social Links -->
                <div class="footer-social-links mb-110 mb-xs-60">
                    <a href="#" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="#" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                    <a href="#" title="Behance" target="_blank"><i class="fa fa-behance"></i></a>
                    <a href="#" title="LinkedIn+" target="_blank"><i class="fa fa-linkedin"></i></a>
                    <a href="#" title="Pinterest" target="_blank"><i class="fa fa-pinterest"></i></a>
                </div>
                <!-- End Social Links -->
                <!-- Footer Text -->
                <div class="footer-text">
                    <!-- Copyright -->
                    <div class="footer-copy font-alt">
                        <a>&copy; Paintpack 2016</a>
                    </div>
                    <!-- End Copyright -->
                    <div class="footer-made">
                        Made with love for great people.
                    </div>
                </div>
                <!-- End Footer Text -->
             </div>
             <!-- Top Link -->
             <div class="local-scroll">
                 <a href="#top" class="link-to-top"><i class="fa fa-caret-up"></i></a>
             </div>
             <!-- End Top Link -->
        </footer>
        <!-- End Foter -->
        </div>
        <!-- JS -->
        <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/SmoothScroll.js"></script>
        <script type="text/javascript" src="js/jquery.scrollTo.min.js"></script>
        <script type="text/javascript" src="js/jquery.localScroll.min.js"></script>
        <script type="text/javascript" src="js/jquery.viewport.mini.js"></script>
        <script type="text/javascript" src="js/jquery.countTo.js"></script>
        <script type="text/javascript" src="js/jquery.appear.js"></script>
        <script type="text/javascript" src="js/jquery.sticky.js"></script>
        <script type="text/javascript" src="js/jquery.parallax-1.1.3.js"></script>
        <script type="text/javascript" src="js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
        <script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
        <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDZWPO1l9mp5m3wkTQMCSUWDdTQazRh2Wo"></script>
        <script type="text/javascript" src="js/gmap3.min.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>
        <script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
        <script type="text/javascript" src="js/jquery.simple-text-rotator.min.js"></script>
        <script type="text/javascript" src="js/all.js"></script>
        <script type="text/javascript" src="js/contact-form.js"></script>
        <script type="text/javascript" src="js/jquery.ajaxchimp.min.js"></script>
    </body>
</html>
