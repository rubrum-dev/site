@extends('common.template')

@section('title')
	Dry Offset
@stop

@section('content')

        <!-- Head Section -->
        <section class="page-section bg-dark-alfa-30 parallax-3" data-background="images/front-end/full-width-images/dryoffset-banner.jpg">
            <div class="relative container align-left">
                <div class="row">
                    <div class="col-md-8">
                        <h1 class="hs-line-11 font-alt mb-20 mb-xs-0">DRY OFFSET</h1>
                        <div class="hs-line-4 font-alt">
                            METALGRAFIA COM QUALIDADE PRECISÃO
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Head Section -->

        <!-- Text Section -->
        <section class="page-section small-section" id="about">
            <div class="container relative">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="section-text align-center">
                            <blockquote>
                                <p>
                                    Dry Offset é um processo de impressão delicado que consiste na transferência de todas
									as cores ao suporte de uma só vez. Amplamente utilizado no mercado de latas de
									alumínio para bebidas.
                                </p>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Text Section -->

        <!-- Divider -->
        <hr class="mt-0 mb-0 "/>
        <!-- End Divider -->

        <!-- Alternative Services Section -->
        <section class="page-section small-section" id="start">
            <div class="container relative">
                <!-- Section Headings -->
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 align-center">
                        <h3 class="font-alt mb-70 mb-sm-40">FERRAMENTAS & SERVIÇOS</h3>
                        <div class="section-text mb-80 mb-xs-40">
                            Para que um projeto com dry offset saia perfeito, a PaintPack oferece ferramentas essenciais
							para que o gestor do produto possa viabilizar e controlar a produção industrial da embalagem.
                        </div>
                    </div>
                </div>
                <!-- End Section Headings -->
				<div class="col-md-5 mb-sm-40">
					<div class="work-full-media mt-0 white-shadow">
						<img src="images/front-end/full-width-images/dryoffset-machine.jpg" alt="" />
					</div>
				</div>
				<div class="col-md-6 col-lg-6">
					<div class="text">
						<h3 class="font-alt mb-30 mb-xxs-10">LATAS DE AMOSTRA</h3>
						<p>
							Dispomos de uma máquina com capacidade de impressão de 6 cores, que entrega uma lata idêntica à que
							será produzida em escala industrial. Somos a primeira empresa a trazer essa tecnologia para o Brasil.<br>
							A lata de amostra viabiliza ajustes antes da produção final, pois revela tanto a fidelidade das as
							cores escolhidas pela criação, quanto as melhores opções para degradês e níveis de transparência ou
							opacidade da tinta sobre o alumínio.
						</p>
					</div>
				</div>
            </div>
        </section>

        <section class="page-section small-section">
			<div class="container relative">
				<div class="col-md-6 col-lg-6">
					<div class="text">
						<h3 class="font-alt mb-30 mb-xxs-10">FLUXOS DE APROVAÇÃO</h3>
						<p>
							Desde o recebimento do arquivo até a versão final, coordenamos um minucioso fluxo de aprovação para que
							nada saia diferente do planejado no que tange à separação de cores, aplicação de códigos de barras e ajustes
							de volume e corpo dos textos legais. Dessa forma garantimos que a arte-final seja entregue sem erros e dessa
							forma otimizando os resultados do projeto.
						</p>
					</div>
				</div>

				<div class="col-md-5 mb-sm-40 col-lg-offset-1">
					<div class="work-full-media mt-0 white-shadow">
						<img src="images/front-end/full-width-images/dryoffset-circle-person.jpg" alt="" />
					</div>
				</div>
			</div>
        </section>

		<section class="page-section small-section">
			<div class="container relative">
				<div class="col-md-5 mb-sm-40">
					<div class="work-full-media mt-0 white-shadow">
						<img src="images/front-end/full-width-images/dryoffset-engine.jpg" alt="" />
					</div>
				</div>

				<div class="col-md-6 col-lg-6 col-lg-offset-1">
					<!-- About Project -->
					<div class="text">
						<h3 class="font-alt mb-30 mb-xxs-10">MATRIZES DE IMPRESSÃO</h3>
						<p>
							Depois de concluir todo o fluxo de aprovação, a PaintPack fornece a gravação da arte-final em placas digitais
							CDI Esko para impressão industrial, e ainda trabalha também com filmes de qualidade para placas analógicas.
						</p>
					</div>
					<!-- End About Project -->
				</div>
			</div>
		</section>

        <!-- Divider -->
        <hr class="mt-0 mb-0 "/>
        <!-- End Divider -->

		<section class="page-section" id="start">
            <div class="container relative">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 align-center">
                        <h3 class="font-alt mb-70 mb-sm-40">DRY OFFSET, NOSSA EXPERTISE!</h3>
                        <div class="section-text mb-80 mb-xs-40">
                            O dry offset demanda muita atenção dos departamentos de marketing e de controle de qualidade devido às
							possíveis e imprevisíveis variações de cor que uma mesma arte pode sofrer no processo de impressão.
							Dentre muitas razões elegemos 6 que que fazem da PaintPack a referência no mercado nacional e internacional.
                        </div>

                    </div>
                </div>
                <div class="alt-service-grid">
                    <div class="row">
                        <div class="col-sm-6 col-md-3 col-lg-3 dryoffset-landing">
                            <div class="alt-service-wrap">
                                <div class="alt-service-item dryoffset-landing">
                                    <h3 class="alt-services-title font-alt">Branding</h3>
                                    Lorem ipsum dolor sit amet,  elit. Vitae gravida nibh. Morbi dignissim nunc at risus convallis.
                                </div>
                            </div>
                            <div class="alt-service-wrap">
                                <div class="alt-service-item dryoffset-landing">
                                    <h3 class="alt-services-title font-alt">Web Design</h3>
                                    Vivamus neque orci, ultricies blandit ultricies vel, semper interdum elit, non placerat suscipit.
                                </div>
                            </div>
                            <div class="alt-service-wrap">
                                <div class="alt-service-item dryoffset-landing">
                                    <h3 class="alt-services-title font-alt">Logo Design</h3>
                                    Lorem ipsum dolor sit amet,  elit. Vitae gravida nibh. Morbi dignissim nunc at risus convallis.
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-6 hidden-xs hidden-sm">
                            <div class="alt-services-image">
                                <img src="images/front-end/full-width-images/dryoffset-color-tin.jpg" alt="" />
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-3 col-lg-3 dryoffset-landing">
                            <div class="alt-service-wrap">
                                <div class="alt-service-item dryoffset-landing">
                                    <h3 class="alt-services-title font-alt">Development</h3>
                                    Morbi dignissim nunc at risus convallis. Semper interdum ultricies elit, non placerat nisi suscipit.
                                </div>
                            </div>
                            <div class="alt-service-wrap">
                                <div class="alt-service-item dryoffset-landing">
                                    <h3 class="alt-services-title font-alt">Photography</h3>
                                    Lorem ipsum dolor sit amet,  elit. Vitae gravida nibh. Morbi dignissim nunc at risus convallis.
                                </div>
                            </div>
                            <div class="alt-service-wrap">
                                <div class="alt-service-item dryoffset-landing">
                                    <h3 class="alt-services-title font-alt">Marketing</h3>
                                    Neque orci, ultricies blandit ultricies vel, semper interdum ultricies elit, non placerat nisi suscipit.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

		<!-- Divider -->
        <hr class="mt-0 mb-0" style="padding: 100px 0;"/>
        <!-- End Divider -->

		<section class="page-section pt-0 pb-0 banner-section bg-dark" data-background="#000000">
            <div class="container relative">
                <div class="row">
                    <div class="col-sm-7">
                        <div class="mt-140 mt-lg-80 mb-140 mb-lg-80">
                            <div class="banner-content">
                                <h3 class="banner-heading font-alt">Academia Paintpack</h3>
                                <div class="banner-decription">
                                    - Revela se as cores finais correspondem com as escolhidas pela criação <br>									- Determina as melhores opções para degradês <br>									- Avalia a relação da transparência ou opacidade da tinta sobre o alumínio
                                </div>
                                <div class="local-scroll">
                                    <a href="#contato" class="btn btn-mod btn-w btn-medium btn-round">Entrar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 banner-image-dryoffset wow fadeInUp">
                        <img src="images/front-end/full-width-images/dryoffset-light.png" alt="" />
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- End Page Wrap -->

@stop
