@extends('common.template')

@section('title')
	Pré-Media
@stop

@section('content')

        <!-- Head Section -->
        <section class="page-section bg-dark-alfa-30 parallax-3" data-background="images/front-end/full-width-images/premedia-banner.jpg">
            <div class="relative container align-left">
                <div class="row">
                    <div class="col-md-8">
                        <h1 class="hs-line-11 font-alt mb-20 mb-xs-0">PRE-MEDIA</h1>
                        <div class="hs-line-4 font-alt">
                            ORGANIZAÇÃO DOS FLUXOS BRAND-TO-PACKAGE
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Head Section -->

        <!-- Text Section -->
        <section class="page-section small-section" id="about">
            <div class="container relative">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="section-text align-center">
                            <blockquote>
                                <p>
                                    Um pequeno erro pode acarretar um grande prejuízo na produção industrial de uma embalagem.
									Além da perda financeira o dano à marca torna-se grave quando não há um controle de qualidade
									eficaz. A PaintPack organiza todos os fluxos brand-to-package que certificam informações
									textuais, legais e garantem  a mesma qualidade de impressão e cores fiéis ao projeto-matriz,
									mesmo quando produzidas em fábricas diferentes.
                                </p>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Text Section -->

        <!-- Divider -->
        <hr class="mt-0 mb-0 "/>
        <!-- End Divider -->

        <!-- Alternative Services Section -->
        <section class="page-section small-section" id="start">
            <div class="container relative">
                <!-- Section Headings -->
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 align-center">
                        <h3 class="font-alt mb-70 mb-sm-40">SERVIÇOS SINCRONIZADOS</h3>
                        <div class="section-text mb-80 mb-xs-40">
                            Nossas armas para coordenar um processo brand-to-package consistem na definição e organização de etapas,
							aliadas a uma equipe multidisciplinar experiente e alta tecnologia. Tudo para entregar o melhor produto
							final gerando economia e agregando qualidade à marca.
                        </div>
                    </div>
                </div>
                <!-- End Section Headings -->
				<div class="col-md-5 mb-sm-40">
					<div class="work-full-media mt-0 white-shadow">
						<img src="images/front-end/full-width-images/premedia-diamonds.jpg" alt="" />
					</div>
				</div>
				<div class="col-md-6 col-lg-6">
					<div class="text">
						<h3 class="font-alt mb-30 mb-xxs-10">Qualidade na Impressão</h3>
						<p>
							A separação de cores é um trabalho minucioso e fundamental para se conseguir transportar um belo layout
							criado, para a impressão industrial da forma correta. Assim, conseguimos certificar que os nossos
							projetos saiam com o mesmo padrão de cor e qualidade para todas as embalagens impressas, mesmo que
							sejam produzidas em fabricas diferentes. Além disso, garantimos que as informações textuais estejam 100%
							corretas, tanto do ponto de vista legal quanto técnico.
						</p>
					</div>
				</div>
            </div>
        </section>

        <section class="page-section small-section">
			<div class="container relative">
				<div class="col-md-6 col-lg-6">
					<div class="text">
						<h3 class="font-alt mb-30 mb-xxs-10">ECONOMIA E EFICIÊNCIA</h3>
						<p>
							Erros de impressão no processo industrial, apesar de inadmissíveis são mais recorrentes do que se espera,
							o que gera um alto prejuízo para o cliente. Outro problema comum são os fluxos de aprovação de layout que
							costumam ser morosos, estendendo os prazos de forma desnecessária. Os nossos serviços de pre-media trabalham
							em sintonia com o foco de minimizar a níveis nulos os possíveis erros no processo, utilizando sistemas web
							e colaboradores específicos para cada projeto.
						</p>
					</div>
				</div>

				<div class="col-md-5 mb-sm-40 col-lg-offset-1">
					<div class="work-full-media mt-0 white-shadow">
						<img src="images/front-end/full-width-images/premedia-coin.jpg" alt="" />
					</div>
				</div>
			</div>
        </section>

		<section class="page-section small-section">
			<div class="container relative">
				<div class="col-md-5 mb-sm-40">
					<div class="work-full-media mt-0 white-shadow">
						<img src="images/front-end/full-width-images/premedia-target.jpg" alt="" />
					</div>
				</div>

				<div class="col-md-6 col-lg-6 col-lg-offset-1">
					<!-- About Project -->
					<div class="text">
						<h3 class="font-alt mb-30 mb-xxs-10">FLUXOS PERSONALIZADOS</h3>
						<p>
							O retrabalho e lentidão de um projeto muitas vezes é causado pela ausência ou falha na comunicação.
							Como sabemos que tempo é um bem precioso, desenvolvemos um sistema com base web, no qual é possível
							controlar o fluxo de aprovação de forma integrada, do início ao fim. Incluímos departamentos, clientes
							e fornecedores num grupo de trabalho eficiente, onde tudo funciona de forma customizada para atender
							as demandas específicas de cada trabalho.
						</p>
					</div>
					<!-- End About Project -->
				</div>
			</div>
		</section>

		<section class="page-section small-section">
			<div class="container relative">
				<div class="col-md-6 col-lg-6">
					<div class="text">
						<h3 class="font-alt mb-30 mb-xxs-10">Organização e Segurança</h3>
						<p>
							Grande parte dos nossos clientes, antes de contratar nossos serviços de pre-media, possuem suas
							artes-finais pulverizadas entre departamentos, agências e fornecedores, o que dificulta inclusive
							saber se a embalagem que está no mercado possui uma arte-final arquivada. Na PaintPack possuímos
							um servidor dedicado para todas as artes do cliente, onde ficam organizadas cronologicamente,
							com backups diários que garantem total segurança para os arquivos. Além disso oferecemos o
							PaintPack Cloud, um serviço de armazenagem das artes na nuvem, que podem ser acessados pelo cliente
							de qualquer dispositivo com acesso à internet.
						</p>
					</div>
				</div>

				<div class="col-md-5 mb-sm-40 col-lg-offset-1">
					<div class="work-full-media mt-0 white-shadow">
						<img src="images/front-end/full-width-images/premedia-safebox.jpg" alt="" />
					</div>
				</div>
			</div>
        </section>

		<!-- Divider -->
        <hr class="mt-0 mb-0" style="padding: 100px 0;"/>
        <!-- End Divider -->

		<section class="page-section pt-0 pb-0 banner-section bg-dark" data-background="#000000">
            <div class="container relative">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mt-140 mt-lg-80 mb-140 mb-lg-80">
                            <div class="banner-content">
                                <h3 class="banner-heading font-alt">Paintpack CLOUD</h3>
                                <div class="banner-decription">
                                    - Suas artes-finais organizadas de forma cronológica<br>
									- Versões atuais e iguais às que estão no mercado<br>
									- Disponível e acessível de qualquer dispositivo com acesso à internet
                                </div>
                                <div class="local-scroll">
                                    <a href="/cloud" target="_blank" class="btn btn-mod btn-w btn-medium btn-round">Conheça</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 banner-image-premedia wow fadeInUp">
                        <img src="images/front-end/full-width-images/premedia-cloud.png" alt="" />
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- End Page Wrap -->

@stop
